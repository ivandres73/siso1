
//2da entrega
void printString();
void readString(char*);
void handleInterrupt21(int, int, int, int);

//3ra entrega
void readFile(char*, char*);
int strcmp(char*, char*, int);
void strcpy(char*, char*, int);
void clearScreen();
void terminate();

//4ta entrega
void deleteFile(char*);
void writeFile(char*, char*, int);
int strlen(char*);

//5ta entrega
void handleTimerInterrupt(int, int);
void initPT();
void executeProgram(char*);

typedef struct ptEntry {
	int isActive;
	int SP;
} ptEntry;

ptEntry processTable[8];
int currentProcess;

#define BLACK			0x0
#define BLUE			0x1
#define GREEN			0x2
#define CYAN			0x3
#define RED				0x4
#define MAGENTA			0x5
#define BROWN			0x6
#define WHITE			0x7

void main() {
	makeInterrupt21();
	initPT();
	makeTimerInterrupt();
	executeProgram("shell");
}

void initPT() {
	int i;
	for (i=0; i < 8; i++) {
		processTable[i].isActive = 0;
		processTable[i].SP = 0xff00;
	}
	currentProcess = 0;
}

void printString(char* s) {
	register short i = 0;
	while ((*(s + i)) != '\0') {
		printChar(*(s + i++));
	}
}

void readString(char* a) {
	char car;
	register short i = 0;
	while (i >= 0) {
		car = readChar();
		if (car == 0xD) {
			a[i] = '\0';
			printChar('\n');
			return;
		}
		else if (car == 0x8) {
			printChar('\b');
			printChar(' ');
			a[i-1] = '\b';
			printChar(a[i-1]);
			i--;
			continue;
		}
		a[i] = car;
		printChar(a[i++]);
	}
}

void handleInterrupt21(int ax, int bx, int cx, int dx) {
	if (ax == 0)
		printString(bx);
	else if (ax == 1)
		readString(bx);
	else if (ax == 2)
		readSector(bx, cx);
	else if (ax == 3)
		readFile(bx, cx);
	else if (ax == 4)
		executeProgram(bx);
	else if (ax == 5)
		terminate();
	else if (ax == 6)
		writeSector(bx, cx);
	else if (ax == 7)
		deleteFile(bx);
	else if (ax == 8)
		writeFile(bx, cx, dx);
	else if (ax == 0xA)
		clearScreen();
	else
		printString("an error message");
}

void readFile(char* fileName, char* buffer) {
	char name[6];
	int conta = 0;
	char tmp[13312];
	char tmpbuffer[512];
	int i;
	readSector(tmpbuffer, 2);
	while (conta <= 15) {
		for (i=0; i < 6; i++)
			name[i] = tmpbuffer[32*conta + i];
		
		if (strcmp(name, fileName, 6)) {
			for (i=6; i < 32; i++) {
				readSector(tmp, tmpbuffer[32*conta + i]);
				strcpy(tmp, buffer, i-6);
			}
			return;
		}

		conta++;
	}
	buffer[0] = '\0';
	syscall_printString("error 404 file not found");
}

int strcmp(char* s1, char* s2, int length) {
	int i;
	for (i=0; i < length; i++) {
		if (s1[i] != s2[i])
			return 0;
	}
	return 1;
}

void strcpy(char* a1, char* a2, int base) {
	int i;
	for (i=0; i < 512; i++)
		a2[base*512 + i] = a1[i];
}

int strlen(char* str) {
	int i;
	for (i=0; str[i] != '\0';)
		i++;
	return i;
}

void executeProgram(char* name) {
	char program[13312];
	int i;
	int segment;

	readFile(name, program);
	if (program[0] == '\0') {
		printString("Program doesn't exist");
		return;
	}

	for(i=0; i < 8; i++) {
		if (processTable[i].isActive == 0) {
			processTable[i].isActive = 1;
			break;
		}
	}
	i += 2;
	segment = i * 0x1000;

	printString("caca");

	for (i=0; i < 13312; i++)
		putInMemory(segment, i, program[i]);

	launchProgram(segment);
}

void clearScreen() {
	int i;
	for (i=0x8000; i < 0x8FA0; ) {
		putInMemory(0XB000, i, ' ');
		i++;
		putInMemory(0XB000, i, CYAN);
		i++;
	}

	pointerInit();
}

void terminate() {
	executeProgram("shell");
}

void deleteFile(char* fileName) {
	char map[512];
	char directory[512];
	int conta;
	char name[6];
	int i;
	int byte;
	readSector(map, 1);
	readSector(directory, 2);
	conta = 0;
	while (conta <= 15) {
		for (i=0; i < 6; i++)
			name[i] = directory[32*conta + i];
		
		if (strcmp(name, fileName, 6)) {
			directory[32*conta] = '\0';

			while (i < 32) {
				byte = directory[32*conta + i];
				if (byte == '\0')
					break;
				map[byte] = '\0';
				i++;
			}
			break;
		}

		conta++;
	}
	writeSector(map, 1);
	writeSector(directory, 2);
}

void writeFile(char* name, char* buffer, int numberOfSectors) {
	int i;
	int j;
	int k;
	int pos;
	int tam;
	int baseBuffer;
	char map[512];
	char directory[512];
	char subBuffer[512];
	readSector(map, 1);
	readSector(directory, 2);
	
	tam = strlen(name);
	for (i=0; i < 16; i++) {//buscando una entry libre en el directorio

		if (directory[i*32] == '\0') {
			for (j=0; j < tam; j++) {
				directory[i*32 + j] = name[j];
			}
			if (tam != 5) {
				for (; j < 6; j++)
					directory[i*32 + j] = '\0';
			}
			pos = i*32;
			break;
		}
	}

	k = 6;
	baseBuffer = 0;
	for (i=0; i < 512 && numberOfSectors > 0; i++) {//buscando sectores libres en el mapa
		if (map[i] == 0x00) {
			map[i] = 0xFF;
			directory[pos + k++] = i;
			for (j=0; j < 512; j++) {
				subBuffer[j] = buffer[baseBuffer*512 + j];
				if (buffer[baseBuffer*512 + j] == '\0')
					break;
			}
			baseBuffer++;
			writeSector(subBuffer, i);
			numberOfSectors--;
		}
	}
	for (; k < 16; k++)
		directory[pos + k] = '\0';
	writeSector(map, 1);
	writeSector(directory, 2);
}

void handleTimerInterrupt(int segment, int sp) {
	//printString("Tic");
	if (segment == 0x2000)
		processTable[0];
	else if (segment == 0x3000)
		processTable[1];
	else if (segment == 0x4000)
		processTable[2];
	else if (segment == 0x5000)
		processTable[3];
	else if (segment == 0x6000)
		processTable[4];
	else if (segment == 0x7000)
		processTable[5];
	else if (segment == 0x8000)
		processTable[6];
	else if (segment == 0x9000)
		processTable[7];

	returnFromTimer(segment, sp);
}