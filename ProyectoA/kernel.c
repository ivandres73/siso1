void printc(short, char);
void puts(short, char*);
void clearScreen();

#define BLACK			0x0
#define BLUE			0x1
#define GREEN			0x2
#define CYAN			0x3
#define RED				0x4
#define MAGENTA			0x5
#define BROWN			0x6
#define WHITE			0x7

void main() {
	short i = 0;
	clearScreen();
	while (1);
}

void clearScreen() {
	register short i;
	for (i = 0; i < 26; i++)
		puts(i, "                                                               ");
}

void printc(short address, char cha) {
	putInMemory(0xB000, 0x8000 + address, cha);
	putInMemory(0xB000, 0x8001 + address, GREEN);
}

void puts(short i, char *str) {
	short pos = 0;
	i = i*0xA0;
	do {
		printc(i, str[pos++]);
		i += 2;
	} while (str[pos] != '\0');
}
