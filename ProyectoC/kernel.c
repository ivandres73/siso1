
//2da entrega
void printString();
void readString(char* a);
void handleInterrupt21(int, int, int, int);

//3ra entrega
void readFile(char*, char*);
int strcmp(char*, char*, int);
void strcpy(char*, char*, int);
void executeProgram(char*, int);
void clearScreen();
void terminate();

#define BLACK			0x0
#define BLUE			0x1
#define GREEN			0x2
#define CYAN			0x3
#define RED				0x4
#define MAGENTA			0x5
#define BROWN			0x6
#define WHITE			0x7

void main() {
	char buffer[13312];
	makeInterrupt21();
	
	executeProgram("shell", 0x2000);
	
}

void printString(char* s) {
	register short i = 0;
	while ((*(s + i)) != '\0') {
		printChar(*(s + i++));
	}
}

void readString(char* a) {
	char car;
	register short i = 0;
	while (i >= 0) {
		car = readChar();
		if (car == 0xD) {
			a[i] = '\0';
			printChar('\n');
			return;
		}
		else if (car == 0x8) {
			printChar('\b');
			printChar(' ');
			a[i-1] = '\b';
			printChar(a[i-1]);
			i--;
			continue;
		}
		a[i] = car;
		printChar(a[i++]);
	}
}

void handleInterrupt21(int ax, int bx, int cx, int dx) {
	if (ax == 0) {
		printString(bx);
	}
	else if (ax == 1) {
		readString(bx);
	}
	else if (ax == 2) {
		readSector(bx, cx);
	}
	else if (ax == 3) {
		readFile(bx, cx);
	}
	else if (ax == 4) {
		executeProgram(bx, cx);
	}
	else if (ax == 5) {
		terminate();
	}
	else if (ax == 0xA) {
		clearScreen();
	}
	else {
		printString("an error message");
	}
}

void readFile(char* fileName, char* buffer) {
	char name[6];
	int conta = 0;
	char tmp[13312];
	char tmpbuffer[512];
	int i;
	readSector(tmpbuffer, 2);
	while (conta <= 15) {
		for (i=0; i < 6; i++)
			name[i] = tmpbuffer[32*conta + i];
		
		if (strcmp(name, fileName, 6)) {
			for (i=6; i < 32; i++) {
				readSector(tmp, tmpbuffer[32*conta + i]);
				strcpy(tmp, buffer, i-6);
			}
			return;
		}

		conta++;
	}
	syscall_printString("error 404 file not found");
}

int strcmp(char* s1, char* s2, int length) {
	int i;
	for (i=0; i < length; i++) {
		if (s1[i] != s2[i])
			return 0;
	}
	return 1;
}

void strcpy(char* a1, char* a2, int base) {
	int i;
	for (i=0; i < 512; i++)
		a2[base*512 + i] = a1[i];
}

void executeProgram(char* name, int segment) {
	char program[13312];
	int i;
	if (segment < 0x1000 || segment%0x1000 != 0 || segment > 0xA000) {
		printString("forbidden segment");
		return;
	}

	readFile(name, program);
	for (i=0; i < 13312; i++)
		putInMemory(segment, i, program[i]);

	launchProgram(segment);
}

void clearScreen() {
	int i;
	for (i=0x8000; i < 0x8FA0; ) {
		putInMemory(0XB000, i, ' ');
		i++;
		putInMemory(0XB000, i, CYAN);
		i++;
	}

	pointerInit();
}

void terminate() {
	executeProgram("shell", 0x2000);
}
