int strcmp(char*, char*, int);

void main() {
    char command[80] ;
    char tmp[6];
    char buffer[13312];
    int i;

    syscall_printString("SHELL>");
    syscall_readString(command);
    syscall_printString("\r");

    for (i=0; i < 6; i++)
        tmp[i] = command[i];
    
    if (strcmp(tmp, "clear", 5)) {
        syscall_clearScreen();
    }
    else if (strcmp(tmp, "type", 4)) {
        for (i=0; i < 6; i++) {
            tmp[i] = command[5+i];
        }
        syscall_readFile(tmp, buffer);
        syscall_printString(buffer);
    }
    else if (strcmp(tmp, "exec", 4)) {
        for (i=0; i < 6; i++) {
            tmp[i] = command[5+i];
        }
        syscall_executeProgram(tmp, 0x3000);
    }
    else {
        syscall_printString("Bad command !");
        syscall_printString("\r\n");
    }
    command[0] = '\0';
    tmp[0] = '\0';
    buffer[0] = '\0';
    syscall_terminate();
}

int strcmp(char* s1, char* s2, int length) {
	int i;
	for (i=0; i < length; i++) {
		if (s1[i] != s2[i])
			return 0;
	}
	return 1;
}