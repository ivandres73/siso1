//1ra entrega
/*void printc(short, char);
void puts(short, char*);
void clearScreen();*/

//2da entrega
void printString();
void readString(char* a);
void handleInterrupt21(int, int, int, int);

#define BLACK			0x0
#define BLUE			0x1
#define GREEN			0x2
#define CYAN			0x3
#define RED				0x4
#define MAGENTA			0x5
#define BROWN			0x6
#define WHITE			0x7


char buffer[512] = 0;
char line[80] = 0;

void main() {

	//clearScreen();
	makeInterrupt21();
	
	loadProgram();
}

/*void clearScreen() {
	register short i;
	for (i = 0; i < 4000; i++)
		printc(i, ' ');
}

void printc(short address, char cha) {
	putInMemory(0xB000, 0x8000 + address, cha);
	putInMemory(0xB000, 0x8001 + address, BLACK);
}

/*void puts(short i, char *str) {
	short pos = 0;
	i = i*0xA0;
	do {
		printc(i, str[pos++]);
		i += 2;
	} while (str[pos] != '\0');
}*/

void printString(char* s) {
	register short i = 0;
	while ((*(s + i)) != '\0') {
		printChar(*(s + i++));
	}
}

void readString(char* a) {
	char car;
	register short i = 0;
	while (1) {
		car = readChar();
		if (car == 0xD) {
			a[i] = '\0';
			printChar('\n');
			return;
		}
		else if (car == 0x8) {
			a[i] = '\b';
			printChar(a[i]);
			i--;
			continue;
		}
		a[i] = car;
		printChar(a[i++]);
	}
}

void handleInterrupt21(int ax, int bx, int cx, int dx) {
	if (ax == 0)
		printString(bx);
	else if (ax == 1)
		readString(bx);
	else if (ax == 2)
		readSector(bx, cx);
	else
		printString("an error message");
}
