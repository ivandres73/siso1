echo "bcc -ansi..."
bcc -ansi -c -o kernel.o kernel.c

echo "as86 kernel.asm..."
as86 kernel.asm -o kernel_asm.o

echo "ld86 -o kernel..."
ld86 -o kernel -d start_asm.o kernel.o kernel_asm.o os_api.o

echo "dd if=kernel..."
dd if=kernel of=floppya.img bs=512 conv=notrunc seek=3

bcc -ansi -c -o test.o test.c
as86 os_api.asm -o os_api.o
ld86 -o testoo -d test.o os_api.o
