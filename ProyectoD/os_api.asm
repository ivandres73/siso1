;syscalls

    .global _syscall_printString
    .global _syscall_readString
    .global _syscall_readSector
    .global _syscall_readFile
    .global _syscall_terminate
    .global _syscall_clearScreen
    .global _syscall_executeProgram
    .global _syscall_writeSector
    .global _syscall_deleteFile
    .global _syscall_writeFile

;syscall_printString(char *str)
_syscall_printString:
    push bp
	mov bp, sp
	push ds

    mov ax, #0
    mov bx, [bp+4]
    int #0x21

    pop ds
	pop bp
    ret

;syscall_readString(char *str)
_syscall_readString:
    push bp
	mov bp, sp
	push ds

    mov ax, #1
    mov bx, [bp+4]
    int #0x21

    pop ds
	pop bp
    ret

;syscall_readSector(char *buffer, int sector)
_syscall_readSector:
    push bp
	mov bp, sp
	push ds

    mov ax, #2
    mov bx, [bp+4]
    mov cx, [bp+6]
    int #0x21

    pop ds
	pop bp
    ret

;syscall_readFile(char* fileName, char* buffer)
_syscall_readFile:
    push bp
    mov bp, sp
    push ds

    mov ax, #3
    mov bx, [bp+4]
    mov cx, [bp+6]
    int #0x21

    pop ds
    pop bp
    ret

;syscall_executeProgram(char* name, int segment)
_syscall_executeProgram:
    push bp
    mov bp, sp
    push ds

    mov ax, #4
    mov bx, [bp+4]
    mov cx, [bp+6]
    int #0x21

    pop ds
    pop bp
    ret

;syscall_terminate()
_syscall_terminate:
    push bp
    mov bp, sp
    push ds

    mov ax, #5
    int #0x21

    pop ds
    pop bp
    ret

;syscall_writeSector(char* array, int sector)
_syscall_writeSector:
    push bp
    mov bp, sp
    push ds

    mov ax, #6
    mov bx, [bp+4]
    mov cs, [bp+6]
    int #0x21

    pop ds
    pop bp
    ret

;syscall_deleteFile(char* name)
_syscall_deleteFile:
    push bp
    mov bp, sp
    push ds

    mov ax, #7
    mov bx, [bp+4]
    int #0x21

    pop ds
    pop bp
    ret

;syscall_writeFile(char* name, char* buffer, int numberOfSectors)
_syscall_writeFile:
    push bp
    mov bp, sp
    push ds

    mov ax, #8
    mov bx, [bp+4]
    mov cx, [bp+6]
    mov dx, [bp+8]
    int #0x21

    pop ds
    pop bp
    ret

;syscall_clearScreen()
_syscall_clearScreen:
    push bp
    mov bp, sp
    push ds

    mov ax, #0xA
    int #0x21

    pop ds
    pop bp
    ret
