int strcmp(char*, char*, int);
void dir();
void readCommand(char*);
int strlen(char*);

void main() {
    char cmd[80];

    syscall_printString("SHELL>");
    syscall_readString(cmd);
    syscall_printString("\r");

    readCommand(cmd);

    cmd[0] = '\0';
    syscall_terminate();
}

int strcmp(char* s1, char* s2, int length) {
	int i;
	for (i=0; i < length; i++) {
		if (s1[i] != s2[i])
			return 0;
	}
	return 1;
}

int strlen(char* str) {
	int i;
	for (i=0; str[i] != '\0';)
		i++;
	return i;
}

void dir() {
    char directory[512];
    char name[6];
    int i;
    int j;
    syscall_readSector(directory, 2);
    for (i=0; i < 16; i++) {
        if (directory[i*32] == '\0') {
            continue;
        }
        else {
            j = 0;
            while (directory[i*32 + j] != '\0' && j < 6) {
                name[j] = directory[i*32 + j];
                j++;
            }
            if (j < 6) {
                for (; j < 6; j++)
                    name[j] = '\0';
            }
            else {
                name[6] = '\0';
            }
            syscall_printString(name);
            syscall_printString("\r\n");
        }
    }
}

void readCommand(char* command) {
    int i;
    int j;//contador para el comando create
    int tam1;
    int tam2;
    int base;//base usada para el comando create
    char buffer[13312];
    char nombre2[6];//nombre para comando copyfile
    char name[6];//nombre para saber el comando, y dspues para obtener el parametro nombre
    char line[512];//subBuffer para el commando create
    for (i=0; i < 6 && command[i] != ' '; i++)
        name[i] = command[i];
    name[i] = '\0';
    
    if (strcmp(name, "clear", 5)) {
        syscall_clearScreen();
    }
    else if (strcmp(name, "type", 4)) {
        for (i=0; i < 6; i++)
            name[i] = command[5+i];
        name[6] = '\0';
        syscall_printString("name of file:");
        syscall_printString(name);
        syscall_printString("\n");
        syscall_readFile(name, buffer);
        syscall_printString(buffer);
        syscall_printString("\r\n");
    }
    else if (strcmp(name, "exec", 4)) {
        for (i=0; i < 6; i++)
            name[i] = command[5+i];
        name[6] = '\0';
        syscall_executeProgram(name, 0x3000);
    }
    else if (strcmp(name, "delete", 6)) {
        for (i=0; i < 6; i++)
            name[i] = command[7+i];
        name[6] = '\0';
        syscall_deleteFile(name);
    }
    else if (strcmp(name, "dir", 3)) {
        dir();
    }
    else if (strcmp(name, "write", 5)) {
        syscall_writeFile("name", "BodyOfTheFile", 1);
    }
    else if (strcmp(name, "copy", 4)) {
        for (i=0; i < 6 && command[i+5] != ' '; i++)
            name[i] = command[i+5];
        
        name[i] = '\0';
        tam1 = strlen(name);
        for (i=0; i < 6 /*&& command[i+tam1+5] != '\0'*/; i++)
            nombre2[i] = command[i+tam1+6];
        
        nombre2[i] = '\0';
        syscall_printString("name of OLD file:");
        syscall_printString(name);
        syscall_printString("\n");

        syscall_printString("name of NEW file:");
        syscall_printString(nombre2);
        syscall_printString("\n");

        syscall_readFile(name, buffer);
        syscall_printString(buffer);
        syscall_writeFile(nombre2, buffer, 1);
    }
    else if (strcmp(name, "create", 6)) {
        base = 0;
        for (i=0; i < 6; i++)
            name[i] = command[i+7];

        j = 0;
        do {
            syscall_readString(line);
            for (i=0; i < 512; i++) {
                if (line[i] == '\0')
                    break;
                if (i == 511)
                    base += 1;
                buffer[base*512 + j++] = line[i];
            }
            syscall_printString("\r");
        } while (line[0] != '\0');
        syscall_writeFile(name, buffer, base+1);
    }
    else {
        syscall_printString("Bad command !");
        syscall_printString("\r\n");
    }
    name[0] = '\0';
    nombre2[0] = '\0';
    buffer[0] = '0';
}